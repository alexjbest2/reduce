def break_ideals(a, b):
    """
    Given two ideals a and b, if d=gcd(a,b) is not 1 or b, returns
    True, (a/d, d, b/d)
    Otherwise, returns False, (a/d, d, b/d)
    
    EXAMPLES::
    
        sage: K.<a> = QuadraticField(-5)
        sage: break_ideals(K.ideal(30), K.ideal(45))
        (True, (Fractional ideal (2), Fractional ideal (15), Fractional ideal (3)))

    """
    d = a+b
    if d == 1 or d == b:
        return False, (a/d, d, b/d)
    return True, (a/d, d, b/d)


def break_ideal_list(lst0, lst1):
    """
    Uses lst0 to break up the ideals in lst1 into as many pieces as possible.
    
    Returns bl, lst2
    where
    all ideals in lst2 divide ideals in lst1,
    all prime divisors of ideals in lst1 divide ideals in lst2,
    bl is True or False, and is True if and only if lst2 contains a non-zero
    ideal not in lst1.
    
    If all elements of lst1 are coprime, then so are all elements of lst1.

    EXAMPLES::
    
        sage: K.<a> = QuadraticField(-5)
        sage: break_ideal_list([K.ideal(30)], [K.ideal(2*3^2*5^3)])
        (True, [Fractional ideal (2), Fractional ideal (3), Fractional ideal (5)])

    """
    ret = False
    while len(lst0) > 0:
        a = lst0.pop()
        lst2 = []
        if a == 0:
            continue
        while len(lst1) > 0:
            b = lst1.pop()
            bl, pieces = break_ideals(a, b)
            if bl:
                if pieces[0] != 1 and not pieces[0] in lst0:
                    lst0.append(pieces[0])
                if pieces[1] != 1 and not pieces[1] in lst0:
                    lst0.append(pieces[1])
                if pieces[2] != 1 and not pieces[2] in lst0:
                    lst0.append(pieces[2])
                if pieces[1] != 1 and not pieces[1] in lst2:
                    lst2.append(pieces[1])
                if pieces[2] != 1 and not pieces[2] in lst2:
                    lst2.append(pieces[2])
                ret = True
            elif b != 1 and not b in lst2:
                lst2.append(b)
        lst1 = lst2
    return ret, lst1


def primes_and_powers(lst):
    """
    Returns lst1 and lst2 such that lst1 contains only primes, lst2 contains
    only non-primes, and every prime dividing an element of lst also divides
    an element of lst1 or lst2.
    None of elements of lst2 are perfect powers, except possibly when
    divisible by the discriminant of the field.
    """
    lst1 = []
    lst2 = []
    for a in lst:
        n = ZZ(a.norm())
        b, e = n.perfect_power()
        lst_a = break_ideal_list([b], [a])[1]
        for c in lst_a:
            # We first test whether c is prime. Note that c.is_prime() calls
            # c.factor() (in Sage 6.2), so we can't do that until we at least
            # know that the norm of c is a prime power.
            if b.is_prime() and c.is_prime():
                lst1.append(c)
            elif c != 1:
                lst2.append(c)
    return lst1, lst2


def ideal_divides_or_factor(a, b_lst):
    """
    Given a number field ideal a and a list b_lst, test if a divides
    every element of b_lst, and return
    non-trivial factors of a where possible.
    
    INPUT: A number field ideal a and a list of such b_lst.
    
    OUTPUT: (d, c), where d is True or False and if d is False, then c is a
    divisor of a.
    
    EXAMPLES::
    
        sage: K.<alpha> = QuadraticField(-5)
        sage: a = K.ideal(2).factor()[0][0]*7
        sage: ideal_divides_or_factor(a, [14, 3, 2])
        (False, Fractional ideal (2, alpha + 1))
        sage: ideal_divides_or_factor(a, [28, 81, a])
        (False, Fractional ideal (1))
        sage: ideal_divides_or_factor(a, [a, 3*a, 3])
        (False, Fractional ideal (1))
        sage: ideal_divides_or_factor(a, [28, 98, a])
        (True, Fractional ideal (1))

    """
    u = Sequence(b_lst + [a]).universe()
    a = u(a)
    b_lst = [u(b) for b in b_lst]
    ret = True
    for b in b_lst:
        if b != 0 and not a.divides(b):
            ret = False
            c = a+b
            if c != 1:
                return False, c
    return ret, u(1)


def ideal_power_divides_or_factor(a, b, n):
    """
    Given number field ideals a and b, test if a^n divides b, and return
    non-trivial factors of a where possible.
    
    INPUT: Number field ideals a and b and a positive integer n.

    OUTPUT: (d, c), where d is True or False and if d is False, then c is a
    divisor of a.

    EXAMPLES::
    
        sage: K.<alpha> = QuadraticField(-5)
        sage: a = K.ideal(2).factor()[0][0]*7
        sage: ideal_power_divides_or_factor(a, 49, 1)
        (False, Fractional ideal (7))
        sage: ideal_power_divides_or_factor(a, 98, 2)
        (True, None)
        sage: ideal_power_divides_or_factor(a, 98, 3)
        (False, Fractional ideal (1))
        sage: ideal_power_divides_or_factor(a, 14/a, 2)
        (False, Fractional ideal (2, alpha + 1))
        sage: ideal_power_divides_or_factor(a, 14/a, 1)
        (False, Fractional ideal (2, alpha + 1))

    """
    for i in range(n):
        if not a.divides(b):
            return False, a + b
        b = b / a
    return True, None


def poly_strip_zero_mod(p, ideal):
    """
    Given a polynomial p over the maximal order of a number field,
    and given an ideal of that number field. Return a polynomial
    congruent to p modulo the ideal, but such that the leading coefficient
    is not in the ideal.
    
    EXAMPLES::
    
        sage: K.<alpha> = QuadraticField(-5)
        sage: P.<x> = K[]
        sage: a = K.ideal(2).factor()[0][0]*7
        sage: poly_strip_zero_mod(14*x^20 + 28*x^10 + 49*x^9 + 14*x^8 + 2*x^7 + 7, a)
        49*x^9 + 14*x^8 + 2*x^7 + 7
    """
    l = p.list()
    while len(l) > 0 and l[-1] in ideal:
        l = l[:-1]
    if len(l) == 0:
        raise ValueError("zero polynomial modulo the ideal")
    return p.parent()(l)


def poly_quo_rem_or_factor(a, b, ideal):
    """
    Given polynomials a and b over the maximal order of a number field,
    and given a number field ideal, divide a by b with remainder modulo ideal,
    or give a non-trivial factor of ideal.
    
    INPUT: Polynomials a and b over a number field, and an ideal of that
    number field.

    OUTPUT: (d, c), where d is True or False.
    If d is True, then c is (q, r),
    where deg(r) < deg(b mod ideal) and a = b*q + r mod ideal.
    If d is False, then c is a non-trivial factor of ideal.

    EXAMPLES::

        sage: K.<alpha> = QuadraticField(-5)
        sage: P.<x> = K[]
        sage: a = K.ideal(2).factor()[0][0]*7
        sage: p = 14*x^20 + 28*x^10 + 49*x^9 + 14*x^8 + 2*x^7 + 7
        sage: poly_quo_rem_or_factor(p, x, a)
        (True, (14*x^19 + 28*x^9 + 49*x^8 + 14*x^7 + 2*x^6, 7))
        sage: poly_quo_rem_or_factor(p, x^3, a)
        (True, (14*x^17 + 28*x^7 + 49*x^6 + 14*x^5 + 2*x^4, 7))
        sage: poly_quo_rem_or_factor(p, 7*x^3+2*x+1, a)
        (False, Fractional ideal (7))
        sage: poly_quo_rem_or_factor(p, 2*x^3+14*x^2, a)
        (False, Fractional ideal (2, alpha + 1))


    """
    b = poly_strip_zero_mod(b, ideal)
    lc = b.leading_coefficient()
    d = ideal + lc
    if d != 1:
        if d == ideal:
            raise RuntimeError
        return False, d
    lc_inv = lc.inverse_mod(ideal)
    b = b * lc_inv
    assert b.leading_coefficient() - 1 in ideal
    b = b.parent()(b.list()[:-1]+[1])
    return True, a.quo_rem(b)


def poly_gcd_or_factor(a, b, ideal):
    """
    Given polynomials a and b over the maximal order of a number field,
    and given a number field ideal, give the gcd modulo ideal or give
    a non-trivial factor of ideal.
    
    INPUT: Polynomials a and b over a number field, and an ideal of that
    number field.

    OUTPUT: (d, c), where d is True or False.
    If d is True, then (c mod ideal) = gcd((a mod ideal), (b mod ideal)).
    If d is False, then c is a non-trivial factor of ideal.

    ALGORITHM: the Euclidean algorithm

    EXAMPLES::
    
        sage: K.<alpha> = QuadraticField(-5)
        sage: P.<x> = K[]
        sage: a = K.ideal(2).factor()[0][0]*7
        sage: p = 14*x^20 + 28*x^10 + 49*x^9 + 14*x^8 + 2*x^7 + 7
        sage: poly_gcd_or_factor(p, x, a)
        (False, Fractional ideal (7))
        sage: poly_gcd_or_factor(p, 7*x^3+2*x+1, a)
        (False, Fractional ideal (2, alpha + 1))
        sage: poly_gcd_or_factor((x+1)*x^2, (x+15)*(x-7), a)
        (False, Fractional ideal (7))
        sage: poly_gcd_or_factor((x+1)*x^2, (x+15)*x, a)
        (True, x^2 + 15*x)
        sage: poly_gcd_or_factor((x+3)*x^2, (x+15)*x, a)
        (False, Fractional ideal (2, alpha + 1))


    """
    d, c = ideal_divides_or_factor(ideal, b.list())
    if d:
        return True, a # as b is zero mod ideal
    if c != 1:
        return False, c

    d, c = poly_quo_rem_or_factor(a, b, ideal)
    if not d:
        # here c is automatically non-trivial
        return False, c

    return poly_gcd_or_factor(b, c[1], ideal)


def multi_poly_gcd_or_factor(lst, ideal):
    """
    As poly_gcd_or_factor, but with a list of polynomials instead of just two.
    
    EXAMPLES::
    
        sage: K.<alpha> = QuadraticField(-5)
        sage: P.<x> = K[]
        sage: a = K.ideal(2).factor()[0][0]*7
        sage: f = (x-7)^6 + 14
        sage: multi_poly_gcd_or_factor([f.derivative(), f.derivative(2)], a)
        (False, Fractional ideal (2, alpha + 1))
        sage: f = (x-1)^6 + 29
        sage: multi_poly_gcd_or_factor([f.derivative(), f.derivative(2)], K.ideal(29))
        (True, 30*x^4 - 120*x^3 + 180*x^2 - 120*x + 30)
        sage: a = K.ideal(29).factor()[0][0]
        sage: multi_poly_gcd_or_factor([f.derivative(), f.derivative(2)], a)
        (True, 30*x^4 - 120*x^3 + 180*x^2 - 120*x + 30)

    """
    if len(lst) == 0:
        return True, 0
    d, c = multi_poly_gcd_or_factor(lst[1:], ideal)
    if d:
        return poly_gcd_or_factor(lst[0], c, ideal)
    return False, c












    







